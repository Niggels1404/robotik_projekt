#!/usr/bin/env python
###################  import  ###########################
import rospy 
from sensor_msgs.msg import Joy				#joystick message type
from geometry_msgs.msg import Twist			#rover message type

############# creation of objects  #####################
move=Twist()

#############  node initialization  ####################
rospy.init_node('teleop', anonymous=True)

##### initialize values ####
move.linear.x = 0	
move.angular.z = 0
teleop = True #flag to override move_base
lss = 1 #linearspeedscaling
ass = 1 #angularspeedscaling
############ definitions of functions ##################
def callback(data):
	global move			

	move.linear.x=(data.axes[1] * lss)
	move.angular.z=(data.axes[3] * ass)
 

#### definition of publisher/subscriber and services ###
rospy.Subscriber("joy", Joy, callback)					#Subscriber from Joystick
pub = rospy.Publisher('cmd_vel', Twist, queue_size=1)			#Publisher to robot

############# main program #############################
rate = rospy.Rate(10)

#--------------endless loop till shut down -------------#
while not rospy.is_shutdown():

	if (move.linear.x != 0) or (move.angular.z != 0) or (teleop == True):
		pub.publish(move)		#publish					
		teleop = True
	if (move.linear.x == 0) and (move.angular.z == 0):
		teleop = False
	rate.sleep()
