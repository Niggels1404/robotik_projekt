#!/usr/bin/env python
###################  import  ###########################
import rospy 
import tf2_ros
import tf_conversions
from geometry_msgs.msg import TransformStamped

############# creation of objects  #####################


#############  node initialization  ####################
rospy.init_node('node_listener', anonymous=True)

##### initialize values ####
tfBuffer = tf2_ros.Buffer()
transform = TransformStamped()
listener = tf2_ros.TransformListener(tfBuffer)	#setup tf listener

rate = rospy.Rate(10.0)				#set spin rate to 10 Hz

while not rospy.is_shutdown():

	#lookup for the newest transform
	try:
		transform = tfBuffer.lookup_transform('camera_link', 'ar_marker_15', rospy.Time(0))
		#get euler angles form quaternions
	#	euler =tf_conversions.transformations.euler_from_quaternion(rot)
	except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
		continue

	#print translation and rotation
	print 'translation: ',transform
#	print 'rotation: ',euler
	print '########'
	rate.sleep()
