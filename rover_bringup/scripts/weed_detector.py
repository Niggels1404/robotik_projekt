#!/usr/bin/env python
###################  import  ###########################
import rospy 
import numpy as np
from sensor_msgs.msg import Image
from rover_bringup.msg import Field_info
import cv2
from cv_bridge import CvBridge, CvBridgeError
import sys
import roslib

############# creation of objects  #####################




img_with_max_weeds =Image()
img_with_overlay_msg=Image()
img_with_overlay=Image()
img_with_overlay_save=Image()
debugimg= Image()
field_info=Field_info()
field_info_current=Field_info()


#parameters
count_red = 0
count_blue = 0
count_red_max= 0
count_blue_max= 0
on_field=False
send=False
count_all_max=0
image_count=0

#HSV ranges to determine red and blue pixels (Hue, Saturation, Value)
#Hue range is 0 to 179 (180 degrees instead of 360-> 1 step = 2 degrees)
lower_red = np.array([0,180,150])
upper_red = np.array([30,255,255])

lower_blue = np.array([98,150,100])
upper_blue = np.array([140,255,255])

#kernel array for erosion
kernel = np.ones((10,10),np.uint8)

# Set up the detector with default parameters.
params = cv2.SimpleBlobDetector_Params()

#Filter by Area
params.filterByArea = True
params.maxArea = 3000
params.minArea = 10

#Thresholds
params.minThreshold = 10
params.maxThreshold = 200

#Circularity
params.filterByCircularity = False
params.minCircularity = 0.1
params.maxCircularity = 1

#Convexity
params.filterByConvexity = False
params.minConvexity = 0.1
params.maxConvexity = 1

#Inertia
params.filterByInertia = False
params.minInertiaRatio = 0.1





detector = cv2.SimpleBlobDetector_create(params)





#############  node initialization  ####################
rospy.init_node('opencv', anonymous=True)

##### initialize values ####

############ definitions of functions ##################
def callback(img_subscribed):

	global img_with_overlay_msg, count_red, count_blue,on_field, send, count_all_max, count_blue_max, count_red_max, img_with_max_weeds,  lower_red, upper_red, lower_blue, upper_blue, img_with_overlay, img_with_overlay_save
	

	bridge = CvBridge()
	frame = bridge.imgmsg_to_cv2(img_subscribed, desired_encoding="rgb8")
	hsv = cv2.cvtColor(frame, cv2.COLOR_RGB2HSV)
 

	


	    
	maskRed = cv2.inRange(hsv, lower_red, upper_red)
	resRed = cv2.bitwise_and(frame,frame, mask = maskRed)

	maskBlue = cv2.inRange(hsv, lower_blue, upper_blue)
	resBlue = cv2.bitwise_and(frame,frame, mask = maskBlue)

	#maskRedErosion = cv2.erode(maskRed,kernel,iterations = 2)
	#maskBlueErosion = cv2.erode(maskBlue,kernel,iterations = 2)

	maskRed = ~maskRed
	maskBlue = ~maskBlue


	keypointsBlue = detector.detect(maskBlue)
	keypointsRed = detector.detect(maskRed)


	im_with_first_keypoints = cv2.drawKeypoints(frame, keypointsBlue, np.array([]), (255,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
	img_with_overlay = cv2.drawKeypoints(im_with_first_keypoints, keypointsRed, np.array([]), (0,255,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

	count_red=len(keypointsRed)
	count_blue=len(keypointsBlue) 



	#convert image type from openCV to ROS message
	img_with_overlay_msg = bridge.cv2_to_imgmsg(img_with_overlay, encoding="rgb8")


	#determine the maximum number of blue and red weeds in one field
	if count_red > count_red_max:
		count_red_max=count_red
	if count_blue > count_blue_max:
		count_blue_max=count_blue

	#if we see at least one weed and were not on a field, we are entering a new field
	if count_red+count_blue > 0 and on_field==False:
		on_field=True

	#if we see no weeds and were already on a field, we are leaving the field -> send field_info message
	if count_red + count_blue == 0 and on_field ==True:
		send=True
		on_field=False
		count_all_max = 0

	#gather an image with the maximum number of weeds
	if count_red + count_blue > count_all_max:
		count_all_max=count_red+count_blue
		img_with_overlay_save=img_with_overlay


#### definition of publisher/subscriber and services ###
rospy.Subscriber("camera/image_raw", Image, callback)
pub_img_overlay = rospy.Publisher('img_with_overlay', Image, queue_size=1)
pub_field_info = rospy.Publisher('field_info', Field_info, queue_size=1)
pub_field_info_current = rospy.Publisher('field_info_current', Field_info, queue_size=1)
############# main program #############################
rate = rospy.Rate(10)

#--------------endless loop till shut down -------------#
while not rospy.is_shutdown():
	
	field_info_current.red = count_red
	field_info_current.blue = count_blue
	pub_img_overlay.publish(img_with_overlay_msg)
	pub_field_info_current.publish(field_info_current)
	if send==True:
		image_count += 1
		rgb = cv2.cvtColor(img_with_overlay_save, cv2.COLOR_RGB2BGR)	
		#save an image of each field
		print cv2.imwrite("/home/ros/test_ws/src/Robotik_Projekt/rover_bringup/field_images/"+str(image_count)+".png", rgb) 	
		
		field_info.blue = count_blue_max
		field_info.red = count_red_max

		on_field=False
		send=False
		count_red_max=0
		count_blue_max=0

		pub_field_info.publish(field_info)
	rate.sleep()



