#!/usr/bin/env python
    
import roslib  
import rospy  
import tf2_ros
import tf_conversions  
  
from sensor_msgs.msg import Imu  
from geometry_msgs.msg import Quaternion  
from geometry_msgs.msg import TransformStamped
  
br = tf2_ros.TransformBroadcaster()  
quat = Quaternion()  
 


def publishTransform(): 
    global quat
    quat.x = 0
    quat.y = 0.707
    quat.z = 0
    quat.w = 0.707
    t = TransformStamped()  
    t.header.stamp = rospy.Time.now()
    t.header.frame_id = "attitude"
    t.child_frame_id = "base_footprint"
    t.transform.translation.x = 0
    t.transform.translation.y = 0
    t.transform.translation.z = 0
    t.transform.rotation.x = quat.x
    t.transform.rotation.y = quat.y
    t.transform.rotation.z = quat.z
    t.transform.rotation.w = quat.w
    br.sendTransform(t)  
  
#def callback(data):  
#    global quat  
#    quat = data.orientation   
#    publishTransform()  
  
if __name__ == '__main__':  
    rospy.init_node('tf_broadcaster') 
    rate = rospy.Rate(50) 
#    rospy.Subscriber("/imu/data", Imu, callback)  
    while not rospy.is_shutdown():
      publishTransform()
      rate.sleep()
