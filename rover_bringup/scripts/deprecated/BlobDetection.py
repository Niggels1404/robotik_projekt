#!/usr/bin/env python
###################  import  ###########################
import rospy 
import numpy as np
from sensor_msgs.msg import Image
import cv2
from cv_bridge import CvBridge, CvBridgeError
import sys
import roslib

############# creation of objects  #####################




img_with_overlay_out=Image()
debugimg= Image()


count_red = 0
count_blue = 0



# Set up the detector with default parameters.
params = cv2.SimpleBlobDetector_Params()

#Filter by Area
params.filterByArea = True
params.maxArea = 3000
params.minArea = 10

#Thresholds
params.minThreshold = 10
params.maxThreshold = 200

#Circularity
params.filterByCircularity = False
params.minCircularity = 0.1
params.maxCircularity = 1

#Convexity
params.filterByConvexity = False
params.minConvexity = 0.1
params.maxConvexity = 1

#Inertia
params.filterByInertia = False
params.minInertiaRatio = 0.1



detector = cv2.SimpleBlobDetector_create(params)





#############  node initialization  ####################
rospy.init_node('opencv', anonymous=True)

##### initialize values ####

############ definitions of functions ##################
def callback(img1):

	global img_with_overlay_out
	global count_red
	global count_blue

	bridge = CvBridge()


	img = bridge.imgmsg_to_cv2(img1, desired_encoding="bgr8")
	
	gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

	out=cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


	b,g,r = cv2.split(img)

	ret1,threshBlue = cv2.threshold(r,10,255,cv2.THRESH_BINARY)
	ret2,threshRed = cv2.threshold(r,230,255,cv2.THRESH_BINARY)
	threshRed = ~threshRed


	keypointsBlue = detector.detect(threshBlue)
	keypointsRed = detector.detect(threshRed)
 


	im_with_keypoints1 = cv2.drawKeypoints(img, keypointsBlue, np.array([]), (0,255,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

	im_with_keypoints = cv2.drawKeypoints(im_with_keypoints1, keypointsRed, np.array([]), (0,255,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)


	count_red=len(keypointsRed)
	count_blue=len(keypointsBlue) 




	img_with_overlay_out = bridge.cv2_to_imgmsg(im_with_keypoints, encoding="bgr8")




#### definition of publisher/subscriber and services ###
rospy.Subscriber("camera/image_raw", Image, callback)
pub = rospy.Publisher('img_with_overlay', Image, queue_size=1)

############# main program #############################
rate = rospy.Rate(10)

#--------------endless loop till shut down -------------#
while not rospy.is_shutdown():

	
	pub.publish(img_with_overlay_out)
	print("Rote Punkte:")
	print(count_red)
	print(" ")
	print("Blaue Punkte:")
	print(count_blue)
	print(" ")
	rate.sleep()
