#!/usr/bin/env python
###################  import  ###########################
import rospy 
import numpy as np
from sensor_msgs.msg import Image
import cv2
from cv_bridge import CvBridge, CvBridgeError
import sys
import roslib

############# creation of objects  #####################




img_with_overlay_out=Image()
debugimg= Image()


count_red = 0
count_blue = 0
count_red_max= 0
count_blue_max= 0
on_field=False
send=False
flag=False
prev_count=0

lower_red = np.array([0,180,150])
upper_red = np.array([30,255,255])

lower_blue = np.array([98,150,100])
upper_blue = np.array([140,255,255])



# Set up the detector with default parameters.
params = cv2.SimpleBlobDetector_Params()

#Filter by Area
params.filterByArea = True
params.maxArea = 3000
params.minArea = 10

#Thresholds
params.minThreshold = 10
params.maxThreshold = 200

#Circularity
params.filterByCircularity = False
params.minCircularity = 0.7
params.maxCircularity = 1

#Convexity
params.filterByConvexity = False
params.minConvexity = 0.1
params.maxConvexity = 1

#Inertia
params.filterByInertia = False
params.minInertiaRatio = 0.1



detector = cv2.SimpleBlobDetector_create(params)





#############  node initialization  ####################
rospy.init_node('opencv', anonymous=True)

##### initialize values ####

############ definitions of functions ##################
def callback(img1):

	global img_with_overlay_out, count_red, count_blue,on_field, send, prev_count, count_blue_max, count_red_max, lower_red, upper_red, lower_blue, upper_blue
	

	bridge = CvBridge()


	frame = bridge.imgmsg_to_cv2(img1, desired_encoding="bgr8")
	
	hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
 

	#Hue range ist 0 bis 179 (immer 2 grad)


	    
	maskRed = cv2.inRange(hsv, lower_red, upper_red)
	resRed = cv2.bitwise_and(frame,frame, mask = maskRed)

	maskBlue = cv2.inRange(hsv, lower_blue, upper_blue)
	resBlue = cv2.bitwise_and(frame,frame, mask = maskBlue)

	maskRed = ~maskRed
	maskBlue = ~maskBlue


	keypointsBlue = detector.detect(maskBlue)
	keypointsRed = detector.detect(maskRed)
 


	im_with_keypoints1 = cv2.drawKeypoints(frame, keypointsRed, np.array([]), (0,255,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

	im_with_keypoints = cv2.drawKeypoints(im_with_keypoints1, keypointsBlue, np.array([]), (0,255,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)


	count_red=len(keypointsRed)
	count_blue=len(keypointsBlue) 




	img_with_overlay_out = bridge.cv2_to_imgmsg(im_with_keypoints, encoding="bgr8")

	if count_red > count_red_max:
		count_red_max=count_red
	if count_blue > count_blue_max:
		count_blue_max=count_blue
	
	if count_red+count_blue > 0:
		on_field=True





	if count_red + count_blue == 0 and on_field ==True:
		send=True
		on_field=False
		prev_count = 0
		





	if count_red + count_blue > prev_count:
		prev_count=count_red+count_blue
		




#### definition of publisher/subscriber and services ###
rospy.Subscriber("camera/image_raw", Image, callback)
pub = rospy.Publisher('img_with_overlay', Image, queue_size=1)

############# main program #############################
rate = rospy.Rate(10)

#--------------endless loop till shut down -------------#
while not rospy.is_shutdown():
	
	
	pub.publish(img_with_overlay_out)
	if send==True:
		print("Rote Punkte:")
		print(count_red_max)
		print(" ")
		print("Blaue Punkte:")
		print(count_blue_max)
		print(" ")
		on_field=False
		send=False
		count_red_max=0
		count_blue_max=0



	rate.sleep()



























"""


#!/usr/bin/env python
###################  import  ###########################
import rospy 
import numpy as np
from sensor_msgs.msg import Image
import cv2
from cv_bridge import CvBridge, CvBridgeError
import sys
import roslib

############# creation of objects  #####################




img_with_overlay_out=Image()
debugimg= Image()


count_red =0
count_blue=0
prev_count=0
on_field=False
send=False
flag=False




#############  node initialization  ####################
rospy.init_node('opencv', anonymous=True)

##### initialize values ####

############ definitions of functions ##################
def callback(img1):
	global img_with_overlay_out, count_red, count_blue,on_field, send, prev_count
	bridge = CvBridge()
	
	img = bridge.imgmsg_to_cv2(img1, desired_encoding="bgr8")
	#img = cv2.imread('/home/ros/test_ws/src/Robotik_Projekt/Kamerabilder/Cropped/4.tif',cv2.IMREAD_COLOR)
	b,g,r = cv2.split(img)



	#contours red
	ret_red,thresh_red = cv2.threshold(r,240,255,cv2.THRESH_BINARY)
	img2_red, contours_red, hierarchy_red = cv2.findContours(thresh_red,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)


	#contours blue
	ret_blue,thresh_blue = cv2.threshold(r,10,255,cv2.THRESH_BINARY)
	thresh_blue=~thresh_blue  #invert binary image
	img2_blue, contours_blue, hierarchy_blue = cv2.findContours(thresh_blue,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)





	maxArea=500

	for i in range(0,contours.size()):
		double area_red = cv2.contourArea(contours_red)
		double area_blue = cv2.contourArea(contours_blue)
		
		if (area > maxArea):
			maxArea = area
			savedContour = i

	
	img_with_overlay1= cv2.drawContours(img, contours_red, -1, (0,255,255), 3)
	img_with_overlay= cv2.drawContours(img_with_overlay1, contours_blue, -1, (255,0,255), 3)
	count_red=len(contours_red)
	count_blue=len(contours_blue) 
	img_with_overlay_out = bridge.cv2_to_imgmsg(img_with_overlay, encoding="bgr8")
	
	if count_red+count_blue > 0:
		on_field=True





	if count_red + count_blue == 0 and on_field ==True:
		send==True
		on_field=False
		prev_count = 0
		





	if count_red + count_blue > prev_count:
		prev_count=count_red+count_blue
		




#### definition of publisher/subscriber and services ###
rospy.Subscriber("camera/image_raw", Image, callback)
pub = rospy.Publisher('img_with_overlay', Image, queue_size=1)

############# main program #############################
rate = rospy.Rate(10)

#--------------endless loop till shut down -------------#
while not rospy.is_shutdown():
	
	pub.publish(img_with_overlay_out)
	print("Rote Punkte:")
	print(count_red)
	print(" ")
	print("Blaue Punkte:")
	print(count_blue)
	print("sendsss")
	print(send)
	print(on_field)
	print(prev_count)
	
	if send==True:
		print("Rote Punkte:")
		print(count_red)
		print(" ")
		print("Blaue Punkte:")
		print(count_blue)
		print(" ")
		on_field=False
		send=False
	
	
	rate.sleep()

"""
