#!/usr/bin/env python
###################  import  ###########################
import rospy


import numpy as np
import cv2


img = cv2.imread('/home/ros/test_ws/src/robotik_projekt/Kamerabilder/Cropped/5.tif', 1)


#print img


gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

out=cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


b,g,r = cv2.split(img)

ret1,thresh1 = cv2.threshold(r,10,255,cv2.THRESH_BINARY)
ret2,thresh2 = cv2.threshold(r,230,255,cv2.THRESH_BINARY)
thresh2 = ~thresh2


# Set up the detector with default parameters.
params = cv2.SimpleBlobDetector_Params()

#Filter by Area
params.filterByArea = False
params.maxArea = 3000
params.minArea = 10

#Thresholds
params.minThreshold = 10
params.maxThreshold = 200

#Circularity
params.filterByCircularity = True
params.minCircularity = 0.1
params.maxCircularity = 1

#Convexity
params.filterByConvexity = True
params.minConvexity = 0.1
params.maxConvexity = 1

#Inertia
params.filterByInertia = False
params.minInertiaRatio = 0.1



detector = cv2.SimpleBlobDetector_create(params)
 
# Detect blobs.
keypoints1 = detector.detect(thresh1)
keypoints2 = detector.detect(thresh2)
 
# Draw detected blobs as red circles.
# cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS ensures the size of the circle corresponds to the size of blob


im_with_keypoints1 = cv2.drawKeypoints(img, keypoints1, np.array([]), (0,255,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

im_with_keypoints2 = cv2.drawKeypoints(img, keypoints2, np.array([]), (0,255,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)



cv2.imshow("Keypoints1", im_with_keypoints1)

cv2.imshow("Keypoints2", im_with_keypoints2)

cv2.waitKey(0)
cv2.destroyAllWindows()



#Ausgabe
print("Anzahl der blauen Kreise:")
print(len(keypoints1))

print("Anzahl der roten Kreise:")
print(len(keypoints2))


#cv2.imshow('img',thresh1)
cv2.waitKey(0)
cv2.destroyAllWindows()








#############  node initialization  ####################
rospy.init_node('opencv', anonymous=True)

##### initialize values ####

############ definitions of functions ##################

#### definition of publisher/subscriber and services ###


############# main program #############################
rate = rospy.Rate(10)

#--------------endless loop till shut down -------------#
#while not rospy.is_shutdown():

	
	

	#rate.sleep()
