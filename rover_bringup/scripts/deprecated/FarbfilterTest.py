#!/usr/bin/env python
import cv2
import numpy as np


frame = cv2.imread('/home/ros/test_ws/src/robotik_projekt/Kamerabilder/Cropped/ErodeTest.png', 1)

hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
 
#Hue range ist 0 bis 179 (immer 2 grad)
lower_red = np.array([0,200,180])
upper_red = np.array([30,255,255])

lower_blue = np.array([98,150,100])
upper_blue = np.array([140,255,255])


kernel = np.ones((10,10),np.uint8)


    
maskRed = cv2.inRange(hsv, lower_red, upper_red)
resRed = cv2.bitwise_and(frame,frame, mask = maskRed)

maskBlue = cv2.inRange(hsv, lower_blue, upper_blue)
resBlue = cv2.bitwise_and(frame,frame, mask = maskBlue)




cv2.imshow('frame',frame)

#cv2.imshow('maskBlue',maskBlue)
#cv2.imshow('resBlue',resBlue)

#cv2.imshow('maskRed',maskRed)
#cv2.imshow('resRed',resRed)
    





#cv2.imshow('maskBlue',maskBlue)

maskRedErosion = cv2.erode(maskRed,kernel,iterations = 2)
maskBlueErosion = cv2.erode(maskBlue,kernel,iterations = 2)



#cv2.imshow('maskBlueErosion',maskBlueErosion)
#cv2.imshow('maskRedErosion',maskRedErosion)


maskRed = ~maskRed
maskBlue = ~maskBlue


# Set up the detector with default parameters.
params = cv2.SimpleBlobDetector_Params()

#Filter by Area
params.filterByArea = True
params.maxArea = 3000
params.minArea = 10

#Thresholds
params.minThreshold = 10
params.maxThreshold = 200

#Circularity
params.filterByCircularity = False
params.minCircularity = 0.1
params.maxCircularity = 1

#Convexity
params.filterByConvexity = False
params.minConvexity = 0.1
params.maxConvexity = 1

#Inertia
params.filterByInertia = False
params.minInertiaRatio = 0.1



detector = cv2.SimpleBlobDetector_create(params)

hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
 
	    
maskRed = cv2.inRange(hsv, lower_red, upper_red)
resRed = cv2.bitwise_and(frame,frame, mask = maskRed)


maskBlue = cv2.inRange(hsv, lower_blue, upper_blue)
resBlue = cv2.bitwise_and(frame,frame, mask = maskBlue)

maskRed = ~maskRed
maskBlue = ~maskBlue


keypointsBlue = detector.detect(maskBlue)
keypointsRed = detector.detect(maskRed)
 


im_with_keypoints = cv2.drawKeypoints(frame, keypointsRed, np.array([]), (0,255,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

im_with_keypoints = cv2.drawKeypoints(im_with_keypoints, keypointsBlue, np.array([]), (0,255,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

#Ausgabe der Positionen. Das pt Atribut ist eine Liste mit 0 = x und 1 = y

print("Positionen von Blau:")
for n in range(len(keypointsBlue)+1):
	x = keypointsBlue[n-1].pt[0]
	y = keypointsBlue[n-1].pt[1]
	print("X:" + str(x) + "	Y:"+ str(y))

print("")

print("Positionen von Rot:")
for n in range(len(keypointsRed)+1):
	x = keypointsRed[n-1].pt[0]
	y = keypointsRed[n-1].pt[1]
	print("X:" + str(x) + "	Y:"+ str(y))

#cv2.imshow('image_with_Keypoints', im_with_keypoints)


k = cv2.waitKey(0)

cv2.destroyAllWindows()

