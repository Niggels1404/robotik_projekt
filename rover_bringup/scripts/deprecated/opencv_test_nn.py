#!/usr/bin/env python
###################  import  ###########################
import rospy 
import numpy as np
from sensor_msgs.msg import Image
import cv2
from cv_bridge import CvBridge, CvBridgeError
import sys
import roslib

############# creation of objects  #####################




img_with_overlay_out=Image()
debugimg= Image()


count_red =0
count_blue=0
prev_count=0
on_field=False
send=False
flag=False




#############  node initialization  ####################
rospy.init_node('opencv', anonymous=True)

##### initialize values ####

############ definitions of functions ##################
def callback(img1):
	global img_with_overlay_out, count_red, count_blue,on_field, send, prev_count
	bridge = CvBridge()
	
	img = bridge.imgmsg_to_cv2(img1, desired_encoding="bgr8")
	#img = cv2.imread('/home/ros/test_ws/src/Robotik_Projekt/Kamerabilder/Cropped/4.tif',cv2.IMREAD_COLOR)
	b,g,r = cv2.split(img)



	#contours red
	ret_red,thresh_red = cv2.threshold(r,240,255,cv2.THRESH_BINARY)
	img2_red, contours_red, hierarchy_red = cv2.findContours(thresh_red,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)


	#contours blue
	ret_blue,thresh_blue = cv2.threshold(r,10,255,cv2.THRESH_BINARY)
	thresh_blue=~thresh_blue  #invert binary image
	img2_blue, contours_blue, hierarchy_blue = cv2.findContours(thresh_blue,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)





	
	
	img_with_overlay1= cv2.drawContours(img, contours_red, -1, (0,255,255), 3)
	img_with_overlay= cv2.drawContours(img_with_overlay1, contours_blue, -1, (255,0,255), 3)
	count_red=len(contours_red)
	count_blue=len(contours_blue) 
	img_with_overlay_out = bridge.cv2_to_imgmsg(img_with_overlay, encoding="bgr8")
	
	if count_red+count_blue > 0:
		on_field=True





	if count_red + count_blue == 0 and on_field ==True:
		send==True
		on_field=False
		prev_count = 0
		





	if count_red + count_blue > prev_count:
		prev_count=count_red+count_blue
		




#### definition of publisher/subscriber and services ###
rospy.Subscriber("camera/image_raw", Image, callback)
pub = rospy.Publisher('img_with_overlay', Image, queue_size=1)

############# main program #############################
rate = rospy.Rate(10)

#--------------endless loop till shut down -------------#
while not rospy.is_shutdown():
	
	pub.publish(img_with_overlay_out)
	print("Rote Punkte:")
	print(count_red)
	print(" ")
	print("Blaue Punkte:")
	print(count_blue)
	print("sendsss")
	print(send)
	print(on_field)
	print(prev_count)
	
	if send==True:
		print("Rote Punkte:")
		print(count_red)
		print(" ")
		print("Blaue Punkte:")
		print(count_blue)
		print(" ")
		on_field=False
		send=False
	
	
	rate.sleep()
