# field_robot_event_master

- ackermann_vehicle (cloned package)
  - ROS packages for simulating a vehicle with Ackermann steering

- rover_bringup
  - config:
    - configuration for rviz
  - launch:
    - file for starting rviz with saved config
    - file for starting joystick teleoperation
    - add new launch files here

- rover_description
  - materials
    - textures of the kinect camera
  - meshes:
    - sensors:
      1. kinect
      2. sick_tim
    - add new meshes for weeding tool here
  - urdf:
    - sensors:
      1. IMU
      2. openni_camera (kinect)
      3. sick_tim
    - rover
    - add new urdf files here

- rover_gazebo
  - launch:
    - simulation.launch starts the whole simulation with ackermann steering, gazebo,..
  - models:
    - change or add models here if necessary
    1. blue_weed
    2. corn
    3. plane_field
    4. red_line
    5. red_weed
  - src:
    - ackermann_cmd_wrapper: listens to cmd_vel and publishes ackermann commands
  - worlds (contains worlds for every challenge)
    1. field_1.erb.world (ruby script) change to this directory in terminale and use command erb field_1.erb.world > field_1.world to generate field_1.world
    2. field_2.erb.world (ruby script) change to this directory in terminale and use command erb field_2.erb.world > field_2.world to generate field_2.world
    3. field_3.erb.world (ruby script) change to this directory in terminale and use command erb field_3.erb.world > field_3.world to generate field_3.world
    4. field_4.erb.world (ruby script) change to this directory in terminale and use command erb field_4.erb.world > field_4.world to generate field_4.world

